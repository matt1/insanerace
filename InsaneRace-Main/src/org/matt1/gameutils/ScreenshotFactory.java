package org.matt1.gameutils;

import java.nio.ByteBuffer;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

/**
 * Take a screengrab of the current window
 * 
 * Based on http://badlogicgames.com/forum/viewtopic.php?t=15158&p=65633
 * 
 * @author Matt
 *
 */
public class ScreenshotFactory {

	public static int[] saveScreenshot(int x, int y, int wi, int he, Game game) {

		Pixmap pixmap = getScreenshot(x, y, wi, he, true, game);
		return pixmapToIntArray(pixmap);
	}
	
	public static Pixmap getScreenshot( int x, int y, int w, int h, boolean flipY, Game game ) {
		long start = System.currentTimeMillis();
        Gdx.gl.glPixelStorei( GL10.GL_PACK_ALIGNMENT, 1 );

        final Pixmap pixmap = new Pixmap( w, h, Format.RGBA8888 );
        ByteBuffer pixels = pixmap.getPixels();
        Gdx.gl.glReadPixels( x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, pixels );

        final int numBytes = w * h * 4;
        byte[] lines = new byte[numBytes];
        if ( flipY ) {
           final int numBytesPerLine = w * 4;
           for ( int i = 0; i < h; i++ ) {
              pixels.position( (h - i - 1) * numBytesPerLine );
              pixels.get( lines, i * numBytesPerLine, numBytesPerLine );
           }
           pixels.clear();
           pixels.put( lines );
        } else {
           pixels.clear();
           pixels.get( lines );
        }
        long duration = (System.currentTimeMillis() - start) / 1000;
        Gdx.app.log("IR", "getScreenshot took " + String.valueOf(duration) + "s");
        return pixmap;
     }
  private static int[] pixmapToIntArray( Pixmap pixmap ) {
	  	long start = System.currentTimeMillis();
        int w = pixmap.getWidth();
        int h = pixmap.getHeight();
        //int source = 0;
        int dest = 0;
        int[] raw = new int[w * h];
        for ( int y = 0; y < h; y++ ) {
           for ( int x = 0; x < w; x++ ) {

              // 32-bit RGBA8888
              int rgba = pixmap.getPixel( x, y );
              raw[dest++] = 0xFF000000 | ( rgba >> 8 );
              //source++;
           }
        }
        
        long duration = (System.currentTimeMillis() - start) / 1000;
        Gdx.app.log("IR", "pixmapToIntArray took  " + String.valueOf(duration) + "s");
        return raw;
     }

}

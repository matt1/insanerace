package org.matt1.gameutils.events;

/**
 * Classes that implement this class can "register" to listen to events.
 * 
 * @author Matt
 *
 */
public interface EventListener {

	/** Process an event that has been raised and do something with it (maybe nothing!) */
	public void processEvent(GameEvent event);
	
}

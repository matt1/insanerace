package org.matt1.gameutils.events;

/**
 * An event from the game
 * 
 * @author Matt
 *
 */
public class GameEvent {

	/** Tag for the event */
	private String tag;
	
	/** Message for the event */
	private Object[] message;

	/**
	 * Create a new event
	 * 
	 * @param tag
	 * @param message
	 */
	public GameEvent(String tag, Object... message) {
		this.tag = tag;
		this.message = message;
	}
	
	public String getTag() {
		return tag;
	}

	public Object[] getMessage() {
		return message;
	}
	
	
	
}

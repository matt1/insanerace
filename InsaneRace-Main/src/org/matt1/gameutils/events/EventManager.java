package org.matt1.gameutils.events;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that handles a centalised, static event receive & distribution in an attempt to avoid
 * spaghetti code disasters.
 * 
 * General approach is that classes register them selves as EventListeners, then classes
 * can raise GameEvents.  Each registered EventListener will get the event and do what it
 * wants to with it (potentially ignore it).
 * 
 * @author Matt
 *
 */
public class EventManager {

	/** Static instance */
	private static EventManager instance;
	
	/**
	 * Get the Event Manager
	 * 
	 * @return
	 */
	public static EventManager get() {
		if (instance == null) {
			instance = new EventManager();
		}
		return instance;
	}
	
	/** List of all listeners */
	public List<EventListener> listeners = new ArrayList<EventListener>();
	
	/**
	 * Empty contructor
	 */
	private EventManager() {
		
	}
		
	/**
	 * Send an event to all listeners
	 * 
	 * @param event
	 */
	public void sendEvent(GameEvent event) {
		for (EventListener listener : listeners) {
			listener.processEvent(event);
		}
	}
	
	/**
	 * Register a new event listener if it has not already been added.
	 * 
	 * @param listener
	 */
	public void addListener(EventListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}
	
}

package org.matt1.gameutils.google;

/**
 * Interface for google play services (scoreboards, achievements etc) as well as social sharing
 * 
 * @author Matt
 *
 */
public interface GooglePlayService {

	public void Login();
	public void LogOut();

	/** get if client is signed in to Google+ */
	public boolean getSignedIn();

	/** submit a score to a leaderboard */
	public void submitScore(int score);

	/** gets the scores and displays them threw googles default widget */
	public void getScores();

	/** gets the score and gives access to the raw score data */
	public void getScoresData();
	
	/** True if signed in */
	public boolean isSignedIn();
	
	/** Shows the leaderboard */
	public void showLeaderboard();
	
	/** Shows the achievements */
	public void showAchievements();
	
	/** SUbmits and achievement */
	public void submitAchievement(String code);
	
	/** Rate the app */
	public void rate();
	
}

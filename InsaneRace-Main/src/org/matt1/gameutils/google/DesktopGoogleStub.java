package org.matt1.gameutils.google;


/**
 * Stub implementation for desktop games
 * 
 * @author Matt
 *
 */
public class DesktopGoogleStub implements GooglePlayService {

	@Override
	public void Login() {
		// TODO Auto-generated method stub

	}

	@Override
	public void LogOut() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean getSignedIn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void submitScore(int score) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getScores() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getScoresData() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isSignedIn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void showLeaderboard() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void showAchievements() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void submitAchievement(String code) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void rate() {
		// TODO Auto-generated method stub		
	}


}

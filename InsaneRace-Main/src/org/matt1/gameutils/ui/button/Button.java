package org.matt1.gameutils.ui.button;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Button that can be tapped
 * 
 * @author Matt
 *
 */
public class Button implements Tappable {

	/** Button texture */
	private Texture texture;
	
	/** Position */
	private int x,y;
	
	/**
	 * Create a new restart button
	 * @param x
	 * @param y
	 */
	public Button(String texturePath) {
		texture = new Texture(Gdx.files.internal(texturePath));
	}
	
	@Override
	public boolean checkClicked(Camera camera) {
		boolean result = false;
		
		Vector3 pos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(pos);
		
		// Check coords were within our area
		if (	pos.x > x && pos.x < (x + texture.getWidth()) && 
				pos.y > y && pos.y < (y + texture.getHeight())) {
			result = true;
		}
		
		return result;
	}

	@Override
	public void draw(int x, int y, SpriteBatch batch) {
		this.x = x;
		this.y = y;
		batch.draw(texture, x, y);

	}

}

package org.matt1.gameutils.ui.button;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Clickable/tappable button
 * 
 * @author Matt
 *
 */
public interface Tappable {

	/** Check if we were clicked or not (uses camera to translate to proper coords) */
	public boolean checkClicked(Camera camera);
	
	/** Draw */
	public void draw(int x, int y, SpriteBatch batch);
	
}

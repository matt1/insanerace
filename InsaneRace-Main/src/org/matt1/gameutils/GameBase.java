package org.matt1.gameutils;

import org.matt1.gameutils.ads.AdServices;
import org.matt1.gameutils.google.GooglePlayService;

import com.badlogic.gdx.Game;

/**
 * Base LibGDX game class.
 * 
 * @author Matt
 *
 */
public abstract class GameBase extends Game {

	/** AdRequestHandler interface */
	protected AdServices adServices;

	/** Google play services interface */
	protected GooglePlayService googlePlayServices;
	
	/**
	 * Creates a new GameBase instance 
	 * 
	 * @param adServices AdServices implementation
	 * @param googlePlayServices Google Play Services implementation
	 */
	public GameBase(AdServices adServices, GooglePlayService googlePlayServices) {
		this.adServices = adServices;
        this.googlePlayServices = googlePlayServices;
        this.googlePlayServices.Login();
	}
	
	/**
	 * Render the game.
	 */
	public void render() {
		super.render(); 
	}

	/**
	 * Clean up anything we're done with.
	 */
	public void dispose() {

	}
}

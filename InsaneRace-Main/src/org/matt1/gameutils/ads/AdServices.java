package org.matt1.gameutils.ads;

/**
 * Can show and hide ads
 * 
 * @author Matt
 *
 */
public interface AdServices {

	/** Show or hide an ad */
	public void showAds(Boolean show);
	
	/** Refresh the ad */
	public void refresh();
	
}

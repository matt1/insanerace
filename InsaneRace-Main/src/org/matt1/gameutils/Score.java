package org.matt1.gameutils;


import java.util.Map;

import org.matt1.gameutils.events.EventManager;
import org.matt1.gameutils.events.GameEvent;
import org.matt1.gameutils.google.GooglePlayService;
import org.matt1.insanerace.Constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;


/**
 * Handles all scoring tasks (including reporting back to Google Play Services)
 * 
 * @author Matt
 *
 */
public class Score {

	private static int score = 0;
	
	/** Key for preferences */
	private static String SCORE_KEY = "bestscore";
	
	/** Key for play count preferences */
	private static String PLAY_COUNT_KEY = "playcount";	
	
	/** Reference to GOoglePlayServices implementation */
	private GooglePlayService googlePlayServices;
	
	/** Contains a mapping of scores to achievement codes for Google */
	private Map<Integer, String> scoreBasedAchievements;
	
	/** Last achievement we got */
	private int lastAchievement = -1;
	
	/**
	 * 
	 * @param googlePlayServices
	 */
	public Score(GooglePlayService googlePlayServices, Map<Integer, String> achievements) {
		this.googlePlayServices = googlePlayServices;
		this.scoreBasedAchievements = achievements;
	}
	
	/**
	 * Increase the score
	 */
	public void increment() {
		score++;
		
		// Check for achievements
		if (scoreBasedAchievements.containsKey(score)) {
			lastAchievement = score;
			googlePlayServices.submitAchievement(scoreBasedAchievements.get(score));
		}
		
	}
	
	/**
	 * Get the current score
	 * @return
	 */
	public int get() {
		return score;
	}
	
	/**
	 * Get score as string
	 * @return
	 */
	public  String asString() {
		return String.valueOf(score);
	}
	
	/**
	 * Reset the score
	 */
	public void reset() {
		score = 0;
		lastAchievement = -1;
	}
	
	private Preferences getPrefs() {
		return  Gdx.app.getPreferences("preferences");
	}
	
	/**
	 * Increment the number of games played
	 */
	public void incrementPlayCount() {
		Preferences prefs = getPrefs();
		int result = 0;
		if (prefs.contains(PLAY_COUNT_KEY)) {
			result = prefs.getInteger(PLAY_COUNT_KEY);
		}
		result++;
		prefs.putInteger(PLAY_COUNT_KEY, result); 
		prefs.flush();
		
		// Work out buckets
		int bucket = result / 50;
		EventManager.get().sendEvent(new GameEvent(Constants.EVENT_GA, 
				Constants.EVENT_GA_PLAY_BUCKET, bucket));
	}
	
	/**
	 * Get previous best score
	 * 
	 * @return
	 */
	public int getPreviousBest() {
		Preferences prefs = getPrefs();
		int result = 0;
		if (prefs.contains(SCORE_KEY)) {
			result = prefs.getInteger(SCORE_KEY);
		}
		return result;
	}
	
	/**
	 * 
	 * @param newBest
	 */
	public void setBestScore(int newBest) {
		Preferences prefs = getPrefs();
		prefs.putInteger(SCORE_KEY, newBest); 
		prefs.flush();
		updateGoogleLeaderBoard();
	}
	
	/**
	 * Update the google play services. 
	 */
	private void updateGoogleLeaderBoard() {
		if (googlePlayServices != null && googlePlayServices.isSignedIn()) {
			googlePlayServices.submitScore(score);
		}
	}
	
	/**
	 * Get the score at which we got the last achievement
	 * @return
	 */
	public int getLastAchievmentScore() {
		return lastAchievement;
	}
}

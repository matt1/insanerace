package org.matt1.insanerace;

/**
 * Contains some constants
 * 
 * @author Matt
 *
 */
public class Constants {

	/** Used for tracking Google Analytics events */
	public static final String EVENT_GA = "trackGaEvent";

	/** Used for sharing */
	public static final String EVENT_SHARE = "share";
	
	/** Used for tracking Google Analytics timer starts */
	public static final String EVENT_GA_TIMING_START = "trackGaTimingStart";
	
	/** Used for tracking Google Analytics timer ends */
	public static final String EVENT_GA_TIMING_END = "trackGaTimingEnd";
	
	/** Event values used for messages */
	public static final String EVENT_GA_GAMESTART = "start";
	public static final String EVENT_GA_GAMEEND = "gameOver";
	public static final String EVENT_GA_RATE = "rate";
	public static final String EVENT_GA_FACEBOOK = "facebook";
	public static final String EVENT_GA_TWITTER = "twitter";
	public static final String EVENT_GA_RESTART = "restart";
	public static final String EVENT_GA_LEADERBOARD = "leaderboard";
	public static final String EVENT_GA_OPTOUT = "optout";
	public static final String EVENT_GA_PLAY_BUCKET = "playbucket";
	public static final String EVENT_GA_CRASH = "carCrash";
}

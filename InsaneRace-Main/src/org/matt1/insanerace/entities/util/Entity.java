package org.matt1.insanerace.entities.util;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Interface for all in-game entities 
 * 
 * @author Matt
 *
 */
public interface Entity {

	/**
	 * Entity should draw itself
	 * @param spriteBatch
	 */
	public void draw(SpriteBatch spriteBatch);
	
	/**
	 * Get the width of this entity
	 * 
	 * @return
	 */
	public int getWidth();
	
	/**
	 * Get the height of this entity
	 * 
	 * @return
	 */
	public int getHeight();
	
	/**
	 * Get the X coordinate of this entity
	 * @return
	 */
	public int getX();
	
	/**
	 * Get the Y coordinate of this entity
	 * 
	 * @return
	 */
	public int getY();
	
}

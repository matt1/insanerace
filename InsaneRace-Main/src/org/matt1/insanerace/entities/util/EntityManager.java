package org.matt1.insanerace.entities.util;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Manager for all in-game entities
 * 
 * @author Matt
 *
 */
public class EntityManager {

	/** Ordered collection of all in-game entities */
	public static List<Entity> entities = new ArrayList<Entity>();
	
	/**
	 * Add an entity to this entity manager
	 * 
	 * @param entity
	 */
	public static void addEntity(Entity entity) {
		entities.add(entity);
	}
	
	/**
	 * Get all in-game entities to draw
	 * @return
	 */
	public static List<Entity> getEntities() {
		return entities;
	}
	
	/**
	 * Instruct all managed entites to draw themselves
	 * 
	 * @param spriteBatch
	 */
	public static void drawAll(SpriteBatch spriteBatch) {
		for (Entity ent : entities) {
			ent.draw(spriteBatch);
		}
	}
	
}

package org.matt1.insanerace.entities;

import org.matt1.insanerace.entities.util.Entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Car entity
 * 
 * @author Matt
 *
 */
public class Car implements Entity {

	/** Rectangle for the car */
	Sprite sprite;
	
	/** Texture for the car */
	Texture carTex;
	
	/** Did we crash? */
	Boolean isCrashed = false;
	
	/** Previous direction */
	private enum Direction {
		UP(0),
		DOWN(180),
		LEFT(270),
		RIGHT(90);

		private int angle;
		private Direction(int angle) {
			this.angle = angle;
		}
		
		public int getAngle() {
			return angle;
		}
	}
	
	/** Direction we're facing */
	private Direction direction;

	/** Direction we were facing */
	private Direction lastDirection;
	
	/** Maximum speed */
	private float maxSpeed = 9f;
	
	/** Default speed for reset */
	private float defaultMaxSpeed = 9;
	
	/** Absolute max speed we cant go past */
	private float terminalVelocity = 16f;
	
	/** Amount we increment speed by each time */
	private float speedIncrement = 1f;
	
	/** Acceleration */
	private float accel = 0.15f;
	
	/** Driving speed */
	private float speed = 0;
	
	/** Are we turning */
	private Boolean turning = false;
	
	/** Have we finished turning */
	//private Boolean turnComplete = false;
	
	/** Turning steps */
	private static final int maxTurns = 6;
	private static final int turnDegrees = 90/maxTurns;
	private static final float turningSpeed = 0.75f;
	
	private int turnCount = 0;
	
	/** Turns we need to make */
	//private List<Vector2> turns = new ArrayList<Vector2>();
	
	private Sound scoreSound;
	
	private Sound crashSound;
	
	/**
	 * Create a new car entity
	 */
	public Car(int startX, int startY) {
		carTex = new Texture(Gdx.files.internal("car.png"));
		
		// Init sprite for car
		sprite = new Sprite(carTex, 0, 0, 64, 64);
		reset(startX, startY);
		
		// Init car audio
		scoreSound = Gdx.audio.newSound(Gdx.files.internal("Short-Score.wav"));
		crashSound = Gdx.audio.newSound(Gdx.files.internal("crash.wav"));
		
	}
	
	/**
	 * Increment the speed of the car up to the maximum velocity
	 */
	public void incrementSpeed() {
		if (maxSpeed < terminalVelocity) {
			maxSpeed += speedIncrement;
		}
		if (maxSpeed > terminalVelocity) {
			maxSpeed = terminalVelocity;
		}
	}
	
	/**
	 * Check to see if the car collided with anything
	 * 
	 * @param tiles
	 * @return
	 */
	public Boolean checkCollision(Array<Rectangle> tiles) {
		Boolean result = false;
		if (isCrashed) {
			result = true;
		} else {
			for (Rectangle tile : tiles) {
				if (sprite.getBoundingRectangle().overlaps(tile)) {
					Gdx.input.vibrate(250);
					crashSound.play(1.0f);
					result = true;
					isCrashed = true;
					break;
				}
			}	
		}
	
		return result;
	}
	
	public Boolean isCrashed() {
		return isCrashed;
	}
	
	public void reset(int startX, int startY) {
		speed = 0;
		maxSpeed = defaultMaxSpeed;
		isCrashed = false;
		lastDirection = direction = Direction.UP;
		sprite.setPosition(startX, startY);
		sprite.setRotation(direction.getAngle());
		//turnComplete = true;
		turning = false;
		turnCount = 0;		
	}
	
	
	/**
	 * Turn left
	 */
	public void turnLeft() {
		if (!turning) {
			scoreSound.play(1.0f);
			//turnComplete = false;
			turning = true;
			lastDirection = direction;
			turnCount = 0;
			if (direction == Direction.UP) {
				direction = Direction.LEFT;
			} else if (direction == Direction.LEFT) {
				direction = Direction.DOWN;
			} else if (direction == Direction.DOWN) {
				direction = Direction.RIGHT;
			} else if (direction == Direction.RIGHT) {
				direction = Direction.UP;
			}
		}
		
	}
	
	/**
	 * Turn right
	 */
	public void turnRight() {
		if (!turning) {
			scoreSound.play(1.0f);
			//turnComplete = false;
			turning = true;
			lastDirection = direction;
			turnCount = 0;
			if (direction == Direction.UP) {
				direction = Direction.RIGHT;
			} else if (direction == Direction.LEFT) {
				direction = Direction.UP;
			} else if (direction == Direction.DOWN) {
				direction = Direction.LEFT;
			} else if (direction == Direction.RIGHT) {
				direction = Direction.DOWN;
			}
		}

	}
	
	/**
	 * Get speed
	 * 
	 * @return
	 */
	public float getSpeed() {
		return speed;
	}
	
	/**
	 * Not turning (no input)
	 */
	public void dontTurn() {
		//turnComplete = false;
	}
	
	/**
	 * Update everything we need to before drawing
	 */
	private void update() {
		if (!isCrashed) {
			// Turn - Angles go anti-clockwise?

			
			speed += accel;
			if (speed > maxSpeed) {
				speed = maxSpeed;
			}
			
			if (!turning) {
				//turnComplete = true;
				sprite.setRotation(-1 * direction.getAngle());
				
				
				// "drive"
				if (direction == Direction.UP) {
					sprite.translateY(speed);
				} else if (direction == Direction.DOWN) {
					sprite.translateY(-speed);
				} else if (direction == Direction.LEFT) {
					sprite.translateX(-speed);
				} else if (direction == Direction.RIGHT) {
					sprite.translateX(speed);
				}
			} else {
				if (lastDirection == Direction.UP) {
					if (direction == Direction.RIGHT) {
						sprite.setRotation((float) (sprite.getRotation() - turnDegrees));
						sprite.translateY((float) (speed * turningSpeed));
						sprite.translateX((float) (speed * turningSpeed));
						turnCount++;
					} else if (direction == Direction.LEFT) {
						sprite.setRotation((float) (sprite.getRotation() + turnDegrees));
						sprite.translateY((float) (speed * turningSpeed));
						sprite.translateX((float) (speed * -turningSpeed));
						turnCount++;
					}
				} else if (lastDirection == Direction.RIGHT) {
					
					if (direction == Direction.UP) {
						sprite.setRotation((float) (sprite.getRotation() + turnDegrees));
						sprite.translateY((float) (speed * turningSpeed));
						sprite.translateX((float) (speed * turningSpeed));
						turnCount++;
					} else if (direction == Direction.DOWN) {
						sprite.setRotation((float) (sprite.getRotation() - turnDegrees));
						sprite.translateY((float) (speed * -turningSpeed));
						sprite.translateX((float) (speed * turningSpeed));
						turnCount++;
					}
					
				} else if (lastDirection == Direction.DOWN) {
					if (direction == Direction.RIGHT) {
						sprite.setRotation((float) (sprite.getRotation() + turnDegrees));
						sprite.translateY((float) (speed * -turningSpeed));
						sprite.translateX((float) (speed * turningSpeed));
						turnCount++;
					} else if (direction == Direction.LEFT) {
						sprite.setRotation((float) (sprite.getRotation() - turnDegrees));
						sprite.translateY((float) (speed * -turningSpeed));
						sprite.translateX((float) (speed * -turningSpeed));
						turnCount++;
					}
				} else if (lastDirection == Direction.LEFT) {
					if (direction == Direction.UP) {
						sprite.setRotation((float) (sprite.getRotation() - turnDegrees));
						sprite.translateY((float) (speed * turningSpeed));
						sprite.translateX((float) (speed * -turningSpeed));
						turnCount++;
					} else if (direction == Direction.DOWN) {
						sprite.setRotation((float) (sprite.getRotation() + turnDegrees));
						sprite.translateY((float) (speed * -turningSpeed));
						sprite.translateX((float) (speed * -turningSpeed));
						turnCount++;
					}
				}
				
				if (turnCount >= maxTurns) {
					turning = false;
				}
			}
		}
		
	}
	
	@Override
	public void draw(SpriteBatch spriteBatch) {
		
		update();
		sprite.draw(spriteBatch);
	}

	@Override
	public int getWidth() {
		return (int) sprite.getWidth();
	}

	@Override
	public int getHeight() {
		return (int) sprite.getHeight();
	}

	@Override
	public int getX() {
		return (int) sprite.getX();
	}

	@Override
	public int getY() {
		return (int) sprite.getY();
	}

}

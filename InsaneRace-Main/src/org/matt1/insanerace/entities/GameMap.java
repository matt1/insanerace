package org.matt1.insanerace.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

/**
 * Tiled (TMX) map class
 * 
 * @author Matt
 *
 */
public class GameMap {

	/** Name of collision layer */
	private static String COLLIISION_LAYER = "Collision";
	
	/** Starting position layer */
	private static String START_LAYER = "Start";
	
	/** Layer for collision detection "Collision" */
	TiledMapTileLayer collisionLayer;
	
	/** Layer for starting position "Start" */
	TiledMapTileLayer startLayer;
	
	/** Map Renderer */
	OrthogonalTiledMapRenderer mapRenderer;
	
	/** The tiled map instance */
	TiledMap map;
	
	/** Filename of map */
	String fileName;
	
	/** Size of tiles */
	int tileSize;
	
	/** Pool for rectangels used in collision detection */
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject () {
			return new Rectangle();
		}
	};
	
	/** Array of rectangles that are potential colliders */
	private Array<Rectangle> tiles = new Array<Rectangle>();
	
	/**
	 * Create a new map
	 * @param mapName
	 */
	public GameMap (String mapName, int tileSize) {
		Gdx.app.log("IR", "Starting Map init");
		fileName = mapName;
		this.tileSize = tileSize;
		loadTMX();
		Gdx.app.log("IR", "Ending Map init");
	}
	
	/**
	 * Load the TMX map
	 */
	private void loadTMX() {
		map = new TmxMapLoader().load(fileName);
		
		// Hide collision layer
		collisionLayer = (TiledMapTileLayer) map.getLayers().get(COLLIISION_LAYER);
		collisionLayer.setVisible(false);
		
		// Get start layer
		startLayer = (TiledMapTileLayer) map.getLayers().get(START_LAYER);
		startLayer.setVisible(false);
		
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		
	}
	
	/**
	 * Get the map 
	 * @return
	 */
	public TiledMap getMap() {
		return map;
	}
	
	/**
	 * Work out the starting position based on the tile position in the start layer
	 * 
	 * @return
	 */
	public Vector2 getStartingPosition() {
		Gdx.app.log("IR", "Starting start position finding");
		Vector2 result = new Vector2();
		
		searchLoop: {
			for (int y = 0; y < startLayer.getHeight(); y++) {
				for (int x = 0; x < startLayer.getWidth(); x++) {
					Cell cell = startLayer.getCell(x, y);
					if (cell != null) {
						result.x = x * tileSize;
						result.y = y * tileSize;
						break searchLoop;
					}
				}
			}
		}
		Gdx.app.log("IR", "Ending start position finding");
		return result;
	}
	
	/**
	 * Converts screen pixels to map cell indexes
	 * @param x
	 * @param y
	 * @return
	 */
	private Vector2 pixelsToCells(int x, int y) {
		Vector2 result = new Vector2();
		
		result.x = x / tileSize;
		result.y = y / tileSize;
		
		return result;
	}
	
	/**
	 * Gets any neat-by collision tiles by looking for any tiles within the range of the current
	 * car position.
	 * 
	 * @param centreX coord to start check from
	 * @param centreY coord to start check from
	 * @param range distance around x & y to check
	 */
	public void getPotentialCollisionTiles(int startX, int startY, int endX, int endY) {
		
		rectPool.freeAll(tiles);
		tiles.clear();

		// Convert pixels to cell space
		Vector2 startCell = pixelsToCells(startX, startY);
		Vector2 endCell = pixelsToCells(endX, endY);
		
		int cellStartX = (int) startCell.x;
		int cellStartY = (int) startCell.y;
		int cellEndX = (int) endCell.x;
		int cellEndY = (int) endCell.y;
		
		for (int y = cellStartY; y <= cellEndY; y++) {
			for (int x = cellStartX; x <= cellEndX; x++) {
				Cell cell = collisionLayer.getCell(x, y);
				if (cell != null) {
					Rectangle rect = rectPool.obtain();
					rect.set(x * tileSize, y * tileSize, tileSize, tileSize);
					tiles.add(rect);
				}
			}
		}

	}
	
	/**
	 * Gets a list of tiles that are potential colliders
	 * @return
	 */
	public Array<Rectangle> getCollisionTiles() {
		return tiles;
	}
	
	/**
	 * Reset the map
	 */
	public void reset() {
		tiles.clear();
	}
	
	/**
	 * Get the sprite batch for the tile renderer
	 * 
	 * @return
	 */
	public SpriteBatch getSpriteBatch() {
		return mapRenderer.getSpriteBatch();
	}
	
	/**
	 * Render the map
	 */
	public void render(OrthographicCamera camera) {
		mapRenderer.setView(camera);
		mapRenderer.render();
	}


	/**
	 * Clean up
	 */
	public void dispose() {
		mapRenderer.dispose();
		map.dispose();
	}
}

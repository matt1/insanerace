package org.matt1.insanerace.entities.ui;

import org.matt1.gameutils.Score;
import org.matt1.insanerace.entities.Car;
import org.matt1.insanerace.resources.Fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * In-game UI (e.g. score etc)
 * 
 * @author Matt
 *
 */
public class InGameUI {

	/** Main Font */
	BitmapFont font;
	
	/** Shadow font */
	BitmapFont shadow;
	
	/** Thumb prints */
	Texture thumbLeft, thumbRight;
	
	/** Tap signs */
	Texture tapRight;
	Texture tapLeft;
	
	int height;
	
	int width;
	
	/** Numer of points before speeding up */
	int pointsToSpeedUp = 3;
	
	/** Score manager */
	private Score score;
	
	/** Pre-calculated with of the touch zones */
	static int TOUCH_LEFT;
	static int TOUCH_RIGHT;
	
	public InGameUI(int height, int width, Score score) {
		initUI();
		this.height = height;
		this.width = width;
		this.score = score;
		
		// Work out touch zone sizes - left is first third, right is final third 
		TOUCH_LEFT = Gdx.graphics.getWidth() / 3;
		TOUCH_RIGHT = TOUCH_LEFT * 2;
	}
	
	/**
	 * Setup the various UI bits and pieces
	 */
	private void initUI() {
		Gdx.app.log("IR", "Starting In Game UI init");
		
		// Fonts
		font = Fonts.normal();
		shadow = Fonts.shadow();

		// Thumbs
		thumbLeft = new Texture(Gdx.files.internal("thumb-left.png"));
		thumbRight =  new Texture(Gdx.files.internal("thumb-right.png"));
		
		// Tap signs
		tapLeft = new Texture(Gdx.files.internal("tap-left.png"));
		tapRight = new Texture(Gdx.files.internal("tap-right.png"));
		
		Gdx.app.log("IR", "Ending In Game UI init");
	}
	
	/**
	 * Draw the UI
	 */
	public void draw(OrthographicCamera camera, SpriteBatch batch) {
		String scoreStr = score.asString();
		
		int scoreX = (int) camera.position.x - (15 * scoreStr.length());
		int scoreY = (int) camera.position.y + (height/2) - 50;
		shadow.draw(batch, scoreStr, scoreX+2, scoreY-2);
		font.draw(batch, scoreStr, scoreX, scoreY);
		
		// Draw thumbs
		if (score.get() < 5) {
			batch.draw(thumbLeft, (int) camera.position.x - (width/2) - 85, 
					camera.position.y - (height/2));
			batch.draw(tapLeft,  (int) camera.position.x - (width/2) + 50, 
					camera.position.y - (height/2) + thumbLeft.getHeight() + 25);
			
			batch.draw(thumbRight, (int) camera.position.x + (width/2) - 175, 
					camera.position.y - (height/2));
			batch.draw(tapRight,  (int) camera.position.x + (width/2) - 125, 
					camera.position.y - (height/2) + thumbLeft.getHeight() + 25);
		}
	}

	
	/**
	 * Was the screen touched in the left most third
	 * @param x
	 * @return
	 */
	private Boolean isTouchedLeft(int x) {
		Boolean result = false;
		if (x < TOUCH_LEFT) {
			result = true;
		}
		return result;
	}

	/**
	 * Was the screen touched in the right most third
	 * @param x
	 * @return
	 */
	private Boolean isTouchedRight(int x) {
		Boolean result = false;
		if (x > TOUCH_RIGHT) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Handle input
	 * @param car
	 */
	public void handleInput(Car car) {
		
		// Speed up as they get better.
		if (score.get() % pointsToSpeedUp == 0 ) {
			car.incrementSpeed();
			Gdx.app.log("IR", "Incrementing car speed.");
		}
		
		if (isTouchedLeft(Gdx.input.getX())) {
			car.turnLeft();
			score.increment();
		} else if (isTouchedRight(Gdx.input.getX())) {
			car.turnRight();
			score.increment();
		} else {
			car.dontTurn();
		}
	}
	
	
	public void reset() {
		score.reset();
	}
	
	/**
	 * Clean up
	 */
	public void dispose() {
		font.dispose();
		shadow.dispose();
		thumbLeft.dispose();
		thumbRight.dispose();
		
	}
	
}

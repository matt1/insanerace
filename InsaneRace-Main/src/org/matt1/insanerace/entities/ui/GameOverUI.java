package org.matt1.insanerace.entities.ui;

import org.matt1.gameutils.Score;
import org.matt1.gameutils.events.EventManager;
import org.matt1.gameutils.events.GameEvent;
import org.matt1.gameutils.google.GooglePlayService;
import org.matt1.gameutils.ui.button.Button;
import org.matt1.insanerace.Constants;
import org.matt1.insanerace.entities.Car;
import org.matt1.insanerace.resources.Fonts;
import org.matt1.insanerace.screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


/**
 * Game Over UI
 * 
 * @author Matt
 *
 */
public class GameOverUI {

	/** Main Font */
	BitmapFont font;
	
	/** Shadow font */
	BitmapFont shadow;
	
	/** End game board */
	Texture scoreBoard;
	
	/** New best score texture */
	Texture newBest;
	
	/** Medal textures */
	Texture bronze;
	Texture silver;
	Texture gold;
	Texture platinum;
	Texture silicon;
	
	/** Race again button */
	Button raceAgain;
	
	/** Leaderboard button */
	Button leaderboard;
	
	/** Rate button */
	Button rate;
	
	/** Twitter button */
	Button twitter;
	
	/** Facebook button */
	Button facebook;
	
	// POsiitons for replay button
	int raceX;
	int raceY;
	
	/** Screen height */
	int height;
	
	/** Screen width */
	int width;
	
	/** Camera from game screen */
	OrthographicCamera camera;
	
	/** Width of score board */
	int scoreBoardWidth = 440;
	
	/** Height of score board */
	int scoreBoardHeight = 220;
	
	/** Scale of text for score board*/
	float smallTextScale = 0.6f;
	
	boolean newHighScore = false;
	
	/** current score used when incrementing the score counter */
	int currentScore = 0;
	
	/** Delay before starting to count score (in seconds) */
	final float SCORE_DELAY = 0.25f;
	float scoreDrawDelay = SCORE_DELAY;
	
	/** Pre-calculated with of the touch zones */
	static int TOUCH_LEFT;
	static int TOUCH_RIGHT;
	
	/** Score manager */
	private Score score;
	
	/** Google play service */
	private GooglePlayService googlePlayService;
	
	public GameOverUI(int height, int width, Score score, GooglePlayService googlePlayService) {
		initUI();
		this.height = height;
		this.width = width;
		this.score = score;
		this.googlePlayService = googlePlayService;
		// Work out touch zone sizes - left is first third, right is final third 
		TOUCH_LEFT = Gdx.graphics.getWidth() / 3;
		TOUCH_RIGHT = TOUCH_LEFT * 2;
	}
	
	/**
	 * Setup the various UI bits and pieces
	 */
	private void initUI() {
		Gdx.app.log("IR", "Starting Game Over UI init");
		
		// Fonts
		font = Fonts.normal();
		shadow = Fonts.shadow();
		
		// Textures
		scoreBoard = new Texture(Gdx.files.internal("score.png"));
		newBest = new Texture(Gdx.files.internal("new-best.png"));
		
		bronze = new Texture(Gdx.files.internal("bronze.png"));
		silver = new Texture(Gdx.files.internal("silver.png"));
		gold = new Texture(Gdx.files.internal("gold.png"));
		platinum = new Texture(Gdx.files.internal("platinum.png"));
		silicon = new Texture(Gdx.files.internal("silicon.png"));
		
		
		// Buttons
		raceAgain = new Button("race.png");
		leaderboard = new Button("leaderboard.png");		
		rate = new Button("rate.png");
		twitter = new Button("twitter.png");
		facebook = new Button("fb.png");
		
		Gdx.app.log("IR", "Ending Game Over UI init");
	}
	
	/**
	 * Draw the UI
	 */
	public void draw(OrthographicCamera camera, SpriteBatch batch, Car car) {
		this.camera = camera;
		
		// Move the camera "down" so crashed car is visible in space above board
		if (camera.position.y > (car.getY() - 110)) {
			camera.position.y -= car.getSpeed() * 2;
			camera.update();
		}
		
		int scoreX = (int) camera.position.x - 160;
		int scoreY = (int) camera.position.y + (height/2) - 100;
		
		shadow.draw(batch, "Crashed!", scoreX + 2, scoreY - 2);
		font.draw(batch, "Crashed!", scoreX, scoreY);
		
		float boardX = camera.position.x - (scoreBoardWidth / 2);
		float boardY = (float) ((int) camera.position.y - (scoreBoardHeight * 0.75));
		batch.draw(scoreBoard, 
				boardX, 
				boardY,
				scoreBoardWidth, scoreBoardHeight);
		

		font.setScale(smallTextScale, smallTextScale);
		shadow.setScale(smallTextScale, smallTextScale);
		
		
		String scoreString = String.valueOf(currentScore);
		String prevBest = String.valueOf(score.getPreviousBest());
		

		if (scoreDrawDelay <= 0) {
			
			shadow.draw(batch, scoreString, boardX + 22, boardY + 158);
			font.draw(batch, scoreString, boardX + 20, boardY + 160);
			
			if (currentScore < score.get()) {
				currentScore ++;
			} else {
				if (currentScore > score.getPreviousBest()) {
					score.setBestScore(score.get());
				}
			}
			
			if (currentScore > score.getPreviousBest() || newHighScore) {
				newHighScore = true;
				prevBest = scoreString;
				batch.draw(newBest, 
						boardX + 120, 
						boardY + 50, 110, 55);
			}
			
			shadow.draw(batch, prevBest, boardX + 22, boardY + 63);
			font.draw(batch, prevBest, boardX + 20, boardY + 65);
		
			raceX = (int) (boardX + 0);
			raceY = (int) (boardY - 100);
			int padding = 30;
			
			raceAgain.draw(raceX, raceY, batch);
			leaderboard.draw(raceX + 128 + padding, raceY, batch);
			rate.draw(raceX + ((128 + padding) * 2), raceY, batch);					
			
			twitter.draw(raceX + 79, raceY - 96, batch);
			facebook.draw(raceX + 128 + padding + 79, raceY - 96, batch);
				
			Texture medal = null;
			// draw medal if we got one.			
			if (score.getLastAchievmentScore() == 10) {
				medal = bronze;
			} else if (score.getLastAchievmentScore() == 20) {
				medal = silver;
			} else if (score.getLastAchievmentScore() == 30) {
				medal = gold;
			} else if (score.getLastAchievmentScore() == 50) {
				medal = platinum;
			} else if (score.getLastAchievmentScore() == 100) {
				medal = silicon;
			}
			
			if (medal != null) {
				batch.draw(medal, boardX + 260, boardY + 62, 137, 137);
			}
			
		} else {
			scoreDrawDelay -= Gdx.graphics.getDeltaTime();
			
		}

		font.setScale(1f, 1f);
		shadow.setScale(1f, 1f);
		
	}

	/**
	 * Handle input
	 */
	public void handleInput(GameScreen screen) {
		// Check for replay button
		if (scoreDrawDelay <= 0) {
			
			if (raceAgain.checkClicked(camera)) {
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_RESTART));
				screen.reset();
			} else if (leaderboard.checkClicked(camera)) {
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_LEADERBOARD));
				googlePlayService.showLeaderboard();				
			} else if (rate.checkClicked(camera)) {
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_RATE));
				this.googlePlayService.rate();				
			} else if(facebook.checkClicked(camera)) {
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_FACEBOOK));
				EventManager.get().sendEvent(new GameEvent(Constants.EVENT_SHARE, ""));
			} else if(twitter.checkClicked(camera)) {
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_TWITTER));
				EventManager.get().sendEvent(new GameEvent(Constants.EVENT_SHARE, ""));
			}
			
		}
	}
	
	/**
	 * Show the UI
	 */
	public void prepare() {
		currentScore = 0;
		newHighScore = false;
		scoreDrawDelay = SCORE_DELAY;
	}
	
	public void reset() {
		score.reset();
	}
	
	/**
	 * Clean up
	 */
	public void dispose() {
		
	}
	
}

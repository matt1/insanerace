package org.matt1.insanerace;

import java.util.HashMap;
import java.util.Map;

import org.matt1.gameutils.GameBase;
import org.matt1.gameutils.ads.AdServices;
import org.matt1.gameutils.google.GooglePlayService;
import org.matt1.insanerace.screens.GameScreen;

import com.badlogic.gdx.Gdx;

/**
 * Insane race game class
 * 
 * @author Matt
 * 
 */
public class InsaneRaceGame extends GameBase {


	/**
	 * Create a new insane race - just calls super constructor
	 * @param adServices
	 * @param googlePlayServices
	 */
	public InsaneRaceGame(AdServices adServices, GooglePlayService googlePlayServices) {
		super(adServices, googlePlayServices);
	}

	/**
	 * Create a new game instance
	 */
	public void create() {
		
		// Create achievements
		Map<Integer, String> achievements = new HashMap<Integer, String>();
		
		achievements.put(10, "CgkI3szz1eIdEAIQAA");	// bronze
		achievements.put(20, "CgkI3szz1eIdEAIQAQ");	// silver
		achievements.put(30, "CgkI3szz1eIdEAIQAw"); // gold
		achievements.put(50, "CgkI3szz1eIdEAIQBA"); // platinum
		achievements.put(100, "CgkI3szz1eIdEAIQBQ"); // silicon
		
		
		this.setScreen(new GameScreen(this, adServices, googlePlayServices, achievements));
	}



}

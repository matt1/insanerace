package org.matt1.insanerace.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Class that handles fonts to avoid game and UI from needing to laod both
 * 
 * @author Matt
 *
 */
public class Fonts {

	/** Main Font */
	static BitmapFont font;
	
	/** Shadow font */
	static BitmapFont shadow;
	
	/**
	 * Get the shadow font
	 * @return
	 */
	public static BitmapFont normal() {
		if (font == null) {
			font = new BitmapFont(Gdx.files.internal("ui/font/text.fnt"));
			font.setScale(1f, 1f);
		}
		return font;		

	}
	
	/** 
	 * Get the normal font
	 * 
	 * @return
	 */
	public static BitmapFont shadow() {
		if (shadow == null) {
			shadow = new BitmapFont(Gdx.files.internal("ui/font/shadow.fnt"));
			shadow.setScale(1f, 1f);
		}
		return shadow;
	}
	
}

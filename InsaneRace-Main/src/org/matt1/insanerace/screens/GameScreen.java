package org.matt1.insanerace.screens;

import java.util.Map;

import org.matt1.gameutils.Score;
import org.matt1.gameutils.ads.AdServices;
import org.matt1.gameutils.events.EventManager;
import org.matt1.gameutils.events.GameEvent;
import org.matt1.gameutils.google.GooglePlayService;
import org.matt1.insanerace.Constants;
import org.matt1.insanerace.InsaneRaceGame;
import org.matt1.insanerace.entities.Car;
import org.matt1.insanerace.entities.GameMap;
import org.matt1.insanerace.entities.ui.GameOverUI;
import org.matt1.insanerace.entities.ui.InGameUI;
import org.matt1.insanerace.entities.util.EntityManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Main screen of the game
 * 
 * @author Matt
 *
 */
public class GameScreen implements Screen {
	
	/** Set up the camera */
	OrthographicCamera camera;
	static int WIDTH = 480;
	static int HEIGHT = 800;
	
	/** Size of the tiles */
	static int TILE_SIZE = 64;
	
	/** Range to check for collisions */
	static int COLLISION_CHECK_RANGE = TILE_SIZE;
	
	/** How many tiles the screen is half-wide */
	static int TILE_HALF_WIDTH;
	
	/** How many tiles the screen is half-high */
	static int TILE_HALF_HEIGHT;
	
	/** Sprite batch used during render */
	SpriteBatch batch; 
	
	/** Car entity */
	Car car;
	
	/** Game map */
	GameMap map;

	/** In Game UI */
	InGameUI inGameUI;
	
	/** Game Over UI */
	GameOverUI gameOverUi;
	
	/** Ad Services interface  */
	AdServices adServices;
	
	/** Google play services interface */
	private GooglePlayService googlePlayServices;
	
	/** Score manager */
	private Score score;
	
	/** Game over */
	Boolean gameOver = false;
	
	public GameScreen(final InsaneRaceGame game, AdServices adServices, 
			GooglePlayService googlePlayServices, Map<Integer, String> achievements) {
		long startTime = System.currentTimeMillis();
		Gdx.app.log("IR", "Starting game init.");
		
		this.adServices = adServices;
		this.googlePlayServices = googlePlayServices;
		
		score = new Score(googlePlayServices, achievements);
		
		// UI init
		inGameUI = new InGameUI(HEIGHT, WIDTH, score);
		gameOverUi = new GameOverUI(HEIGHT, WIDTH, score, googlePlayServices);
		
		// Load map
		map = new GameMap("race.tmx", TILE_SIZE);
		
		// Add the car
		reset();
		EntityManager.addEntity(car);
	
		// Init camera
		camera = new OrthographicCamera();
		camera.setToOrtho(false, WIDTH, HEIGHT);
		camera.update();
		
		// Init sprite batch
		batch = map.getSpriteBatch();
		
		long total = System.currentTimeMillis() - startTime;
		Gdx.app.log("IR", "Ending game init (" + String.valueOf(total) + "ms)");
		
	}
	
	
	@Override
	public void render(float delta) {
		
		// Clear screen
		Gdx.gl.glClearColor(0.76f,0.88f,0.41f,1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		// Update camera
		if (!car.isCrashed()) {
			camera.position.set(car.getX(), car.getY(), 0);
			camera.update();
		}
		
		// Rendering ==================================================================
		// Draw the game map
		map.render(camera);

		batch.begin();
		
		// Sprites
		EntityManager.drawAll(batch);
		
		// UI
		if (car.isCrashed()) {
			if (!gameOver) {
				gameOver = true;
				adServices.showAds(true);
				gameOverUi.prepare();
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_GAMEEND, score.get()));	
				String crash = String.valueOf(car.getX()) + "," + String.valueOf(car.getY());
				EventManager.get().sendEvent(
						new GameEvent(Constants.EVENT_GA, Constants.EVENT_GA_CRASH, crash));
				googlePlayServices.submitScore(score.get());
			}
			gameOverUi.draw(camera, batch, car);
		} else {
			inGameUI.draw(camera, batch);
		}
				
		batch.end();
		// Rendering End ==============================================================
		
		// Check collisions
		if (car.checkCollision(map.getCollisionTiles())) {
			//Gdx.app.log("IR", "crashed");
		} else {
			map.getPotentialCollisionTiles(car.getX(), car.getY(), car.getX() + TILE_SIZE, 
					car.getY() + TILE_SIZE);
		}
		
		// Handle touch input
		if (Gdx.input.justTouched()) {
			if (car.isCrashed()) {
				gameOverUi.handleInput(this);
			} else {				
				inGameUI.handleInput(car);
			}
		}
		

	}

	/**
	 * Reset everything back to starting conditions
	 */
	public void reset() {		
		adServices.showAds(false);
		inGameUI.reset();
		gameOver = false;
		map.reset();
		Vector2 start = map.getStartingPosition();
		score.incrementPlayCount();
		if (car == null) {
			car = new Car((int) start.x, (int) start.y);
		} else {
			car.reset((int) start.x, (int) start.y);
		}
		
	}
	
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		inGameUI.dispose();
		map.dispose();
	}

}

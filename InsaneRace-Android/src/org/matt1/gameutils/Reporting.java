package org.matt1.gameutils;

import org.matt1.gameutils.events.EventListener;
import org.matt1.gameutils.events.EventManager;
import org.matt1.gameutils.events.GameEvent;
import org.matt1.insanerace.Constants;

import android.content.Context;

import com.badlogic.gdx.Gdx;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

/**
 * Handles reporting events to GA
 * 
 * @author Matt
 *
 */
public class Reporting implements EventListener {

	/** Static instance */
	private static Reporting instance;
	
	/** Easy tracker to use */
	EasyTracker tracker;
	
	/**
	 * Initialise the instance
	 * @param context
	 */
	public static void initInstance(Context context) {
		instance = new Reporting(context);
	}
	
	/** Keep track of opt-out status */
	private Boolean optedOut = false;
	
	/**
	 * Get instance - should be intialised first!
	 * @return
	 */
	public static Reporting getInstance() {
		return instance;
	}
	
	/**
	 * Create a new instance
	 * 
	 * @param context
	 */
	private Reporting(Context context) {
		tracker = EasyTracker.getInstance(context);
		EventManager.get().addListener(this);
	}
	
	/**
	 * Log an event
	 * @param action
	 */
	private void gameEvent(String action) {
		gameEvent(action, null);
	}
	
	/** Opt out from tracking */
	private void optOut() {
		optedOut = true;
		long[] vibe = {0,200,300,200,300,200};
		Gdx.input.vibrate(vibe, -1);
	}
	
	/**
	 * Log an event if not opted out
	 * @param action
	 */
	private void gameEvent(String action, String value) {		
		if (!optedOut) {
			// category, action, label, value
			try {
			tracker.send(MapBuilder.createEvent("game-event", action, value, null).build());
			} catch (Exception e) {
				// nop
			}
		}
	}
	
	/**
	 * Record game start.
	 */
	public void gameStart() {
		gameEvent("start");
	}
	
	/**
	 * View leaderboard
	 */
	public void viewLeaderboard() {
		gameEvent("leaderboard");
	}
	
	/**
	 * View rating
	 */
	public void viewRating() {
		gameEvent("rate");
	}
	
	public void gameRestart() {
		gameEvent("restart");
	}
	
	public void shared(String network) {
		gameEvent("shared", network);
	}
	
	public void gameOver(int score) {
		gameEvent("gameOver", String.valueOf(score));
	}
	
	public void playBucket(int bucket) {
		gameEvent("gamePlaysBucket-50xN", String.valueOf(bucket));
	}
	
	public void crashEvent(String crashCoords) {
		gameEvent("crash",crashCoords);
	}

	@Override
	public void processEvent(GameEvent event) {
		if (event.getTag().equals(Constants.EVENT_GA)) {
			String s = event.getMessage()[0].toString();
			
			if (s.equals(Constants.EVENT_GA_GAMESTART)) {
				gameStart();
			} else if (s.equals(Constants.EVENT_GA_GAMEEND)) {
				gameOver((Integer) event.getMessage()[1]);
			} else if (s.equals(Constants.EVENT_GA_RESTART)) {
				gameRestart();
			} else if (s.equals(Constants.EVENT_GA_FACEBOOK)) {
				shared("facebook");
			} else if (s.equals(Constants.EVENT_GA_TWITTER)) {
				shared("twitter");
			} else if (s.equals(Constants.EVENT_GA_RATE)) {
				viewRating();
			} else if (s.equals(Constants.EVENT_GA_LEADERBOARD)) {
				viewLeaderboard();
			} else if (s.equals(Constants.EVENT_GA_OPTOUT)) {
				optOut();
			} else if (s.equals(Constants.EVENT_GA_PLAY_BUCKET)) {
				playBucket((Integer) event.getMessage()[1]);
			} else if (s.equals(Constants.EVENT_GA_CRASH)) {
				crashEvent(event.getMessage()[1].toString());
			}
		}
		
	}
}

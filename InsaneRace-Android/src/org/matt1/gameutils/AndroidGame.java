package org.matt1.gameutils;

import org.matt1.gameutils.ads.AdServices;
import org.matt1.gameutils.events.EventListener;
import org.matt1.gameutils.events.EventManager;
import org.matt1.gameutils.google.GooglePlayService;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;

/**
 * Class for basic LibGDX + Google Play Services + AdMob android game
 * 
 * @author Matt
 *
 */
public abstract class AndroidGame extends AndroidApplication implements AdServices, 
	GooglePlayService,	GameHelperListener, EventListener {

	/** Time to wait before refreshing ads (in ms) */
	private static final int AD_REFRESH = 30000;
	
	/** Layout for handling ads */
	protected RelativeLayout layout;
	
	/** AdView for AdMob */
	protected AdView adView;
	
	/** If ads should be shown */
    protected final int SHOW_ADS = 1;
    
    /** If ads should not be shown */
    protected final int HIDE_ADS = 0;
	
	/** Refresh count for throttling ad refresh requests */
	protected long lastRefresh;
	
	/** Game Helper for google play */
	protected GameHelper gameHelper;
	
	/** Client for Google game stuff */
	protected GoogleApiClient client;

    /** Debugging tag */
    protected static String TAG;
    
    /** Handler for showing and hiding ads */
    @SuppressLint("HandlerLeak")
	protected Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case SHOW_ADS:
                {
                    adView.setVisibility(View.VISIBLE);
                    break;
                }
                case HIDE_ADS:
                {
                    adView.setVisibility(View.GONE);
                    break;
                }
            }
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		initGoogle();
		
		// Initialise reporting and register with event handler
		Reporting.initInstance(this);
		Reporting.getInstance().gameStart();
		
		// Register to handle events
		EventManager.get().addListener(this);
		
		super.onCreate(savedInstanceState);
		
	}

	/**
	 * Initialises the Google Play Services setup
	 */
	protected void initGoogle() {
		gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
		gameHelper.enableDebugLog(true);

		gameHelper.setup(this);
		client = gameHelper.getApiClient();
		client.connect();
	}
	
	/**
	 * Initialise LibGDX and creates a new new view for the given Game object
	 */
	protected View initLibGDX(Game game) {
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useAccelerometer = false;
		cfg.useCompass = false;
		cfg.useGL20 = false;
		
		return initializeForView(game, cfg);
	}
	
	/**
	 * Setup the AdMob view for handlings ads
	 */
	protected void setupViews(View libgdxView) {
		layout = new RelativeLayout(this);
		layout.addView(libgdxView);
		
		RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT, 
				RelativeLayout.LayoutParams.WRAP_CONTENT);
	    adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
	    adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		
        adView = new AdView(this);
        adView.setAdUnitId("ca-app-pub-6880147666253331/6006633807");
        adView.setAdSize(AdSize.BANNER);
        adView.setVisibility(View.GONE);
       
        refresh();
        
        // Add in new ad view
	    layout.addView(adView, adParams);
	    setContentView(layout);
	}

	
	/**
	 * Show ads
	 * 
	 * @param show True to show ads, false otherwise
	 */
	@Override
	public void showAds(Boolean show) {
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
		if (show) {
			refresh();
		}
	}

	/**
	 * Refresh the ads.  Will not refresh if less than the timeout amount
	 */
	@Override
	public void refresh() {
		long now = System.currentTimeMillis();
		if (lastRefresh == 0 || lastRefresh - now > AD_REFRESH) {
			AdRequest adRequest = new AdRequest.Builder()        
	    	.addTestDevice("252DBDED224596F5A501E5F3661A52EE").build();
			adView.loadAd(adRequest);
			lastRefresh = System.currentTimeMillis();
		} else {
			Gdx.app.log(TAG, "skipping ad refresh due to recent refresh");
		}
	}

	@Override
	public void onSignInFailed() {
		Gdx.app.log(TAG, "Sign in to Google Play Services failed");
	}

	@Override
	public void onSignInSucceeded() {
		Gdx.app.log(TAG, "Sign in to Google Play Services ok");
	}
	
	@Override
	protected void onStart() {
	    super.onStart();
	    gameHelper.onStart(this);
	}

	@Override
	protected void onStop() {
	    super.onStop();
	    gameHelper.onStop();
	}

	@Override
	protected void onActivityResult(int request, int response, Intent data) {
	    super.onActivityResult(request, response, data);
	    gameHelper.onActivityResult(request, response, data);
	}

	@Override
	public void Login() {
		try {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					gameHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex) {
			Gdx.app.log(TAG, "Login to Google Play Services exception " + ex.getMessage());
		}
	}

	@Override
	public void LogOut() {
		try {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					gameHelper.signOut();
				}
			});
		} catch (final Exception ex) {
			Gdx.app.log(TAG, "Logout from Google Play Services exception " + ex.getMessage());
		}
	}

	@Override
	public boolean getSignedIn() {
		return gameHelper.isSignedIn();
	}

	@Override
	public void submitScore(int score) {
		try {
			Games.Leaderboards.submitScore(client, "CgkI3szz1eIdEAIQAg", score);
		} catch (Exception e) {
			Gdx.app.log(TAG, "Submit score exception " + e.getMessage());
		}
	}

	@Override
	public void getScores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getScoresData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSignedIn() {
		return gameHelper.isSignedIn();
	}

	@Override
	public void showLeaderboard() {
		startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
				gameHelper.getApiClient(), "CgkI3szz1eIdEAIQAg"), 1);
		
	}

	@Override
	public void showAchievements() {
		startActivityForResult(Games.Achievements.getAchievementsIntent(
				gameHelper.getApiClient()), 2);
	}
	
	@Override
	public void submitAchievement(String code) {
		try {
			Games.Achievements.unlock(gameHelper.getApiClient(), code);
		} catch (Exception e) {
			Gdx.app.log(TAG, "Unlock achievements exception " + e.getMessage());
		}
	}

}

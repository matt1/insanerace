package org.matt1.insanerace;

import java.io.File;
import java.io.OutputStream;

import org.matt1.gameutils.AndroidGame;
import org.matt1.gameutils.ScreenshotFactory;
import org.matt1.gameutils.ads.AdServices;
import org.matt1.gameutils.events.EventListener;
import org.matt1.gameutils.events.GameEvent;
import org.matt1.gameutils.google.GooglePlayService;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;

/**
 * Class for basic LibGDX + Google Play Services + AdMob android game
 * 
 * @author Matt
 * 
 */
public class InsaneRace extends AndroidGame implements AdServices,
		GooglePlayService, GameHelperListener, EventListener {

	Game race;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		TAG = "InsaneRace";

		super.onCreate(savedInstanceState);

		// Create new Race game and use to initialise GDX for Android
		race = new InsaneRaceGame(this, this);

		View libgdxView = initLibGDX(race);
		setupViews(libgdxView);
	}

	@Override
	public void rate() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		// Try Google play
		intent.setData(Uri.parse("market://details?id=org.matt1.insanerace"));
		startActivity(intent);
	}

	/**	
	 * Save the image
	 * 
	 * @param colors
	 * @param width
	 * @param height
	 * @return
	 */
	public File saveJPG( int[] colors, int width, int height) {
		// http://badlogicgames.com/forum/viewtopic.php?t=15158&p=65633
		long start = System.currentTimeMillis();
		FileHandle fh;
		String path = Gdx.files.getExternalStoragePath();
		FileHandle handle = Gdx.files.absolute(path);
		fh = new FileHandle(handle+"/Pictures/"+"screenshot.jpg");
		Bitmap bitmap = Bitmap.createBitmap(colors, width, height, Bitmap.Config.ARGB_8888);
		OutputStream stream = fh.write( false ); 
		bitmap.compress( CompressFormat.JPEG, 85, stream );	   
        long duration = (System.currentTimeMillis() - start) / 1000;
        Gdx.app.log("IR", "savePNG took  " + String.valueOf(duration) + "s");
		return fh.file();
	}
		
	/**
	 * Show the sharing dialog
	 * 
	 * @param message
	 * @param network
	 */
	private void share(String message, String network) {
						
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, message);
		int[] image = ScreenshotFactory.saveScreenshot(0, 0, Gdx.graphics.getWidth(), 
				Gdx.graphics.getHeight(), race);
		
		File f = saveJPG(image,Gdx.graphics.getWidth(), 
				Gdx.graphics.getHeight());
		if (f != null) {
			Uri fileUri = Uri.fromFile(f);
			sendIntent.putExtra(Intent.EXTRA_STREAM,  fileUri);
		}		
		
		sendIntent.setType("image/*");
		startActivity(Intent.createChooser(sendIntent, "Send to"));

		
	}

	@Override
	public void processEvent(GameEvent event) {
		if (event.getTag().equals(Constants.EVENT_SHARE)) {
			share("Check out my Insane Race score!", null);
		}
		
	}


}
